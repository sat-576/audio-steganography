from Crypto.Cipher import AES

from HH import bh
def get_key(passphrase):
    key = bh(passphrase.encode())
    iv = bh(passphrase.encode(), size=8)
    return (key, iv)

def encrypt(data, passphrase):
    (key, iv) = get_key(passphrase)
    while len(data) % 16 != 0:
        data += " "
    obj = AES.new(key, AES.MODE_CBC, iv)
    return obj.encrypt(data)

def decrypt(encrypted_data, passphrase):
    (key, iv) = get_key(passphrase)
    obj = AES.new(key, AES.MODE_CBC, iv)
    return obj.decrypt(encrypted_data).strip()



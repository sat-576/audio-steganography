#helping the AESC find hash
from hashlib import blake2b, sha256

def s256(data):
    hashit = sha256()
    hashit.update(data)
    return hashit.hexdigest()


def bh(data, s=16):            #blake hash
    hashit = blake2b(digest_size=s)
    hashit.update(data)
    return hashit.hexdigest()


# Audio Steganography

To hide a file 

python stegano.py hide <FILE> <AUDIO> CODE>
FILE -the file one wants to hide inside AUDIO file.
AUDIO is the original audio file used for steganography. Its in .wav format.
CODE is the password required during retrieval

TO decrypt on other side 
python stegano.py retrieve <AUDIO> <CODE>
AUDIO is the input file
CODE is the password used to hide file

In case of no file or wrong password user gets `ValueError`


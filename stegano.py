import ntpath
import click
import sys
import math
from AESC import encrypt, decrypt
import struct
import wave
from HH import s256
import pytaglib
@click.group()
def cli():
    pass

@click.command()
@click.argument("file_to_hide", type=click.Path(exists=True))
@click.argument("audio_file_for_hiding", type=click.Path(exists=True))
@click.argument("passphrase")
def hide(file_to_hide, audio_file_for_hiding, passphrase):

    sound = wave.open(audio_file_for_hiding, "r")

    params = sound.getparams()
    num_channels = sound.getnchannels()
    sample_width = sound.getsampwidth()
    num_frames = sound.getnframes()
    num_samples = num_frames * num_channels

    input_data = open(file_to_hide, "rb").read()
    hash_input_data = sha256_hash(input_data)

    input_data = encrypt(input_data.decode(), passphrase)

    filesize = len(input_data)
    num_lsb = math.ceil(filesize * 8 / num_samples)
    if (num_lsb > 4):
        raise ValueError("Input file too large to hide, "
                         "max byte to hide is {}"
                         .format((num_samples * num_lsb) // 8))

    if (sample_width == 1):  
        fmt = "{}B".format(num_samples)
       
        mask = (1 << 8) - (1 << num_lsb)
     
        min_sample = -(1 << 8)
    elif (sample_width == 2): 
        fmt = "{}h".format(num_samples)
       
        mask = (1 << 15) - (1 << num_lsb)
    
        min_sample = -(1 << 15)
    else:
     
        raise ValueError("File has an unsupported bit-depth")


    raw_data = list(struct.unpack(fmt, sound.readframes(num_frames)))
    sound.close()

    data_index = 0
    sound_index = 0


    values = []
    buffer = 0
    buffer_length = 0
    done = False

    while(not done):
        while (buffer_length < num_lsb and data_index // 8 < len(input_data)):

            buffer += (input_data[data_index // 8] >> (data_index % 8)
                       ) << buffer_length
            bits_added = 8 - (data_index % 8)
            buffer_length += bits_added
            data_index += bits_added

        current_data = buffer % (1 << num_lsb)
        buffer >>= num_lsb
        buffer_length -= num_lsb

        while (sound_index < len(raw_data) and
               raw_data[sound_index] == min_sample):

            values.append(struct.pack(fmt[-1], raw_data[sound_index]))
            sound_index += 1

        if (sound_index < len(raw_data)):
            current_sample = raw_data[sound_index]
            sound_index += 1

            sign = 1
            if (current_sample < 0):

                current_sample = -current_sample
                sign = -1

            altered_sample = sign * ((current_sample & mask) | current_data)

            values.append(struct.pack(fmt[-1], altered_sample))

        if (data_index // 8 >= len(input_data) and buffer_length <= 0):
            done = True

    while(sound_index < len(raw_data)):

        values.append(struct.pack(fmt[-1], raw_data[sound_index]))
        sound_index += 1

    sound_steg = wave.open("output.wav", "w")
    sound_steg.setparams(params)
    sound_steg.writeframes(b"".join(values))
    sound_steg.close()

    output_song = taglib.File("output.wav")
    output_song.tags["STEGO"] = "1"
    output_song.tags["STEGO_SIZE"] = str(filesize)
    output_song.tags["STEGO_CONTENT_SHA256"] = hash_input_data
    output_song.tags["STEGO_FILE_NAME"] = ntpath.basename(file_to_hide)
    output_song.tags["STEGO_LSB"] = str(num_lsb)
    output_song.save()


@click.command()
@click.argument("audio_file", type=click.Path(exists=True))
@click.argument("passphrase")
def retrieve(audio_file, passphrase):


    input_song = taglib.File(audio_file)
    if (not int(input_song.tags["STEGO"][0])):
        raise ValueError("No file is hidden inside")

    file_name = input_song.tags["STEGO_FILE_NAME"][0]
    bytes_to_recover = int(input_song.tags["STEGO_SIZE"][0])
    content_sha256 = input_song.tags["STEGO_CONTENT_SHA256"][0]
    num_lsb = int(input_song.tags["STEGO_LSB"][0])

    input_song.save()

    sound = wave.open(audio_file, "r")

    num_channels = sound.getnchannels()
    sample_width = sound.getsampwidth()
    num_frames = sound.getnframes()
    num_samples = num_frames * num_channels

    if (sample_width == 1):  
        fmt = "{}B".format(num_samples)
 
        min_sample = -(1 << 8)
    elif (sample_width == 2):
        fmt = "{}h".format(num_samples)
 
        min_sample = -(1 << 15)
    else:

        raise ValueError("File has an unsupported bit-depth")


    raw_data = list(struct.unpack(fmt, sound.readframes(num_frames)))

    mask = (1 << num_lsb) - 1
    output_file = open(file_name, "wb+")

    data = bytearray()
    sound_index = 0
    buffer = 0
    buffer_length = 0

    while (bytes_to_recover > 0):

        next_sample = raw_data[sound_index]
        if (next_sample != min_sample):

            buffer += (abs(next_sample) & mask) << buffer_length
            buffer_length += num_lsb
        sound_index += 1

        while (buffer_length >= 8 and bytes_to_recover > 0):
     
            current_data = buffer % (1 << 8)
            buffer >>= 8
            buffer_length -= 8
            data += struct.pack('1B', current_data)
            bytes_to_recover -= 1

    data = decrypt(bytes(data), passphrase)

    if (not (sha256_hash(data) == content_sha256)):
        raise ValueError("Wrong passphrase")

    output_file.write(data)
    output_file.close()


cli.add_command(hide)
cli.add_command(retrieve)

if __name__ == '__main__':
    cli()
